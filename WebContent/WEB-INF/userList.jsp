<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>
<%
// 検索条件のHashMap
// JavaBeansを使わないと、やはり拙い作りになる。
HashMap<String, String> searchCondition = (HashMap<String, String> )request.getAttribute("searchCondition");

// 検索結果のList
ArrayList<HashMap<String, String>> list = (ArrayList<HashMap<String, String>> )session.getAttribute("userlist");

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1>ユーザー一覧画面</h1>
<form action="<%= request.getContextPath() %>/UserList" method="post">
ID:<input type="text" name="userid" size="30" maxlength="512" value="<%= searchCondition.get("USER_ID") %>"><br>
姓:<input type="text" name="lastname" size="30" maxlength="256" value="<%= searchCondition.get("LAST_NAME") %>"><br>
名:<input type="text" name="firstname" size="30" maxlength="256" value="<%= searchCondition.get("FIRST_NAME") %>"><br>
姓（かな）:<input type="text" name="lastnamekana" size="30" maxlength="256" value="<%= searchCondition.get("LAST_NAME_KANA") %>"><br>
名（かな）:<input type="text" name="firstnamekana" size="30" maxlength="256" value="<%= searchCondition.get("FIRST_NAME_KANA") %>"><br>
年齢:<input type="text" name="age" size="5" maxlength="3" value="<%= searchCondition.get("AGE") %>"><br>
<input type="submit" name="search" value="検索">
<input type="submit" name="register" value="新規登録">
</form>
<form action="<%= request.getContextPath() %>/Menu" method="post">
<input type="submit" name="pageback" value="メニューへ">
</form>

<%
if(list != null) {
%>
<form action="<%= request.getContextPath() %>/UserList" method="post">
<table>
<tr>
<th>選択</th>
<th>ユーザーID</th>
<th>パスワード</th>
<th>姓</th>
<th>名</th>
<th>姓（かな）</th>
<th>名（かな）</th>
<th>年齢</th>
</tr>
<%
//listの件数分だけ、HashMapの各値をtd値として格納
int i = 0;
for(HashMap<String, String> map : list) {
%>
<tr>
<td><input type="radio" name="userselect" value=<%= i %> <% if (i == 0) { %>checked<% } %>></td>
<td><%= map.get("USER_ID") %></td>
<td><% /* パスワードはフィルタリング */if (map.get("PASSWORD") != null) %>******</td>
<td><%= map.get("LAST_NAME") %></td>
<td><%= map.get("FIRST_NAME") %></td>
<td><%= map.get("LAST_NAME_KANA") %></td>
<td><%= map.get("FIRST_NAME_KANA") %></td>
<td><%= map.get("AGE") %></td>
</tr>
<%
i++;
}
%>
</table>
<input type="submit" name="update" value="編集">
<input type="submit" name="delete" value="削除">
</form>
<%
}
%>
</body>
</html>