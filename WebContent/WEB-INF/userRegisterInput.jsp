<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.HashMap" %>
<%
// 編集対象ユーザーデータ（HashMap）
// JavaBeansを使わないと、やはり拙い作りになる。
HashMap<String, String> userselected = (HashMap<String, String> )session.getAttribute("userselected");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1>ユーザー新規登録画面</h1>
<form action="<%= request.getContextPath() %>/UserRegisterConfirm" method="post">
ID:<input type="text" name="userid" size="30" maxlength="512" value="<%= userselected.get("USER_ID") %>"><br>
パスワード:<input type="password" name="password" size="30" maxlength="256" value="<%= userselected.get("PASSWORD") %>"><br>
姓:<input type="text" name="lastname" size="30" maxlength="256" value="<%= userselected.get("LAST_NAME") %>"><br>
名:<input type="text" name="firstname" size="30" maxlength="256" value="<%= userselected.get("FIRST_NAME") %>"><br>
姓（かな）:<input type="text" name="lastnamekana" size="30" maxlength="256" value="<%= userselected.get("LAST_NAME_KANA") %>"><br>
名（かな）:<input type="text" name="firstnamekana" size="30" maxlength="256" value="<%= userselected.get("FIRST_NAME_KANA") %>"><br>
年齢:<input type="text" name="age" size="5" maxlength="3" value="<%= userselected.get("AGE") %>"><br>
<input type="submit" value="確認">
</form>
<form style="display: inline" action="<%= request.getContextPath() %>/UserList" method="post">
<input type="submit" value="戻る">
</form>
</body>
</html>