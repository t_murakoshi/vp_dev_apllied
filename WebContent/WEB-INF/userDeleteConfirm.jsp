<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.HashMap" %>
<%
// 編集対象ユーザーデータ（HashMap）
// JavaBeansを使わないと、やはり拙い作りになる。
HashMap<String, String> userselected = (HashMap<String, String> )session.getAttribute("userselected");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1>ユーザー削除確認画面</h1>
<form action="<%= request.getContextPath() %>/UserDeleteComplete" method="post">
ID:<%= userselected.get("USER_ID") %><br>
パスワード:********<br>
姓:<%= userselected.get("LAST_NAME") %><br>
名:<%= userselected.get("FIRST_NAME") %><br>
姓（かな）:<%= userselected.get("LAST_NAME_KANA") %><br>
名（かな）:<%= userselected.get("FIRST_NAME_KANA") %><br>
年齢:<%= userselected.get("AGE") %><br>
<input type="submit" value="削除">
</form>
<form action="<%= request.getContextPath() %>/Menu" method="post">
<input type="submit" value="戻る">
</form>
</body>
</html>