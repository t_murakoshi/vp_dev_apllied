package vp_dev;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Login
 */
@WebServlet("/UserRegisterComplete")
public class UserRegisterComplete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserRegisterComplete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//GETの利用は不可とする
		String filename = "/WEB-INF/userRegisterNG.jsp";	//エラー画面
		RequestDispatcher dispatcher = request.getRequestDispatcher(filename);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");	//POSTした値の文字コードをUTF-8にする

		@SuppressWarnings("unchecked")
		HashMap<String, String> userselected = (HashMap<String, String> )request.getSession().getAttribute("userselected");

		String userid = userselected.get("USER_ID");		//セッション上のuserid
		String password = userselected.get("PASSWORD");	//セッション上のpassword
		String lastname = userselected.get("LAST_NAME");	//セッション上のlastname
		String firstname = userselected.get("FIRST_NAME");	//セッション上のfirstname
		String lastnamekana = userselected.get("LAST_NAME_KANA");		//セッション上のlastnamekana
		String firstnamekana = userselected.get("FIRST_NAME_KANA");	//セッション上のfirstnamekana
		String age = userselected.get("AGE");	//セッション上のage

		request.getSession().removeAttribute("userselected");	//もういらないので削除

		String filename = "/WEB-INF/userRegisterNG.jsp";	//とりあえずNG画面に遷移するように設定
		Connection conn = null;	//finallyで処理するので、ここで定義
		PreparedStatement stmt = null;	//finallyで処理するので、ここで定義

		String dbuser = "root";
		String dbpassword = "password";	//★たぶん皆さんのパスワードはadminなので、adminに変えてください

		try {
			Class.forName("com.mysql.jdbc.Driver");

			//ConnectionとStatementの取得
			conn = DriverManager.getConnection("jdbc:mysql://localhost/VP_DEV?serverTimezone=JST&allowPublicKeyRetrieval=true&useSSL=false", dbuser, dbpassword);

			conn.setAutoCommit(false);

			//日時をjava.sql.Timestampに変換
			ZonedDateTime zonedDatetime
			= ZonedDateTime.now(ZoneId.of("Asia/Tokyo"));
			Timestamp timestamp = Timestamp.from(zonedDatetime.toInstant());

			StringBuffer sb = new StringBuffer();

			//SQL文作成
			sb.append("INSERT INTO USER");
			sb.append("(USER_ID, LAST_NAME, FIRST_NAME, LAST_NAME_KANA, FIRST_NAME_KANA, AGE, CREATE_DT, UPDATE_DT)");
			sb.append("VALUES");
			sb.append("(?,?,?,?,?,?,?,?)");
			sb.append(";");

			stmt = conn.prepareStatement(sb.toString());

			//パラメータ埋め込み
			stmt.setString(1, userid);
			stmt.setString(2, lastname);
			stmt.setString(3, firstname);
			stmt.setString(4, lastnamekana);
			stmt.setString(5, firstnamekana);
			stmt.setInt(6, Integer.parseInt(age));
			stmt.setTimestamp(7, timestamp);
			stmt.setTimestamp(8, timestamp);

			//SQLの実行
			int userCnt = stmt.executeUpdate();

			stmt.close();

			sb = new StringBuffer();

			//SQL文作成
			sb.append("INSERT INTO LOGIN");
			sb.append("(USER_ID, PASSWORD, CREATE_DT, UPDATE_DT)");
			sb.append("VALUES");
			sb.append("(?,?,?,?)");
			sb.append(";");

			stmt = conn.prepareStatement(sb.toString());

			//パラメータ埋め込み
			stmt.setString(1, userid);
			stmt.setString(2, password);
			stmt.setTimestamp(3, timestamp);
			stmt.setTimestamp(4, timestamp);

			//SQLの実行
			int loginCnt = stmt.executeUpdate();

			if (userCnt > 0 && loginCnt > 0) {
				conn.commit();
				filename = "/WEB-INF/userRegisterComplete.jsp";	//両テーブル1件登録できたのでOK
			}
			else {
				//おそらくここには来ないが、念のため
				conn.rollback();
			}

		}
		catch (Exception e) {
			//rollback処理
			try {
				conn.rollback();
			} catch (Exception e2) {
				//処理のしようがないので、何もしない
			}
			e.printStackTrace();
		}
		finally {
			//close処理
			try {
				stmt.close();
				conn.close();
			}
			catch (Exception e)
			{
				//処理のしようがないので、何もしない
			}

		}

		//OKまたはNGへ遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher(filename);
		dispatcher.forward(request, response);
	}

}
