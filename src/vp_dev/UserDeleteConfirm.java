package vp_dev;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Menu
 */
@WebServlet("/UserDeleteConfirm")
public class UserDeleteConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//リダイレクトして呼ぶ場合GETが動くので、こちらにロジックを記載

		//ユーザー情報がセッションに保持されていることが前提で動作するので、ない場合はエラー
		if (request.getSession().getAttribute("userselected") == null) {
			//ブラウザで戻るを使うな的なエラー画面に飛んだ方がスマートだが、とりあえずExceptionスロー
			throw new ServletException("ユーザー情報がセッションにありません");
		}

		//遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/userDeleteConfirm.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
