package vp_dev;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Login
 */
@WebServlet("/UserUpdateComplete")
public class UserUpdateComplete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateComplete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//GETの利用は不可とする
		String filename = "/WEB-INF/UserUpdateNG.jsp";	//エラー画面
		RequestDispatcher dispatcher = request.getRequestDispatcher(filename);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");	//POSTした値の文字コードをUTF-8にする

		@SuppressWarnings("unchecked")
		HashMap<String, String> userselected = (HashMap<String, String> )request.getSession().getAttribute("userselected");

		String userid = userselected.get("USER_ID");		//セッション上のuserid
		String password = userselected.get("PASSWORD");	//セッション上のpassword
		String lastname = userselected.get("LAST_NAME");	//セッション上のlastname
		String firstname = userselected.get("FIRST_NAME");	//セッション上のfirstname
		String lastnamekana = userselected.get("LAST_NAME_KANA");		//セッション上のlastnamekana
		String firstnamekana = userselected.get("FIRST_NAME_KANA");	//セッション上のfirstnamekana
		String age = userselected.get("AGE");	//セッション上のage

		request.getSession().removeAttribute("userselected");	//もういらないので削除

		String filename = "/WEB-INF/userUpdateNG.jsp";	//とりあえずNG画面に遷移するように設定
		Connection conn = null;	//finallyで処理するので、ここで定義
		PreparedStatement stmt = null;	//finallyで処理するので、ここで定義

		String dbuser = "root";
		String dbpassword = "password";	//★たぶん皆さんのパスワードはadminなので、adminに変えてください

		try {
			Class.forName("com.mysql.jdbc.Driver");

			//ConnectionとStatementの取得
			conn = DriverManager.getConnection("jdbc:mysql://localhost/VP_DEV?serverTimezone=JST&allowPublicKeyRetrieval=true&useSSL=false", dbuser, dbpassword);

			conn.setAutoCommit(false);

			//日時をjava.sql.Timestampに変換
			ZonedDateTime zonedDatetime
			= ZonedDateTime.now(ZoneId.of("Asia/Tokyo"));
			Timestamp timestamp = Timestamp.from(zonedDatetime.toInstant());

			StringBuffer sb = new StringBuffer();

			//SQL文作成
			sb.append("UPDATE USER SET ");
			sb.append("LAST_NAME = ?,");
			sb.append("FIRST_NAME = ?,");
			sb.append("LAST_NAME_KANA = ?,");
			sb.append("FIRST_NAME_KANA = ?,");
			sb.append("AGE = ?,");
			sb.append("UPDATE_DT = ?");
			sb.append("WHERE USER_ID = ?");
			sb.append(";");

			stmt = conn.prepareStatement(sb.toString());

			//パラメータ埋め込み
			stmt.setString(1, lastname);
			stmt.setString(2, firstname);
			stmt.setString(3, lastnamekana);
			stmt.setString(4, firstnamekana);
			stmt.setInt(5, Integer.parseInt(age));
			stmt.setTimestamp(6, timestamp);
			stmt.setString(7, userid);

			//SQLの実行
			int userCnt = stmt.executeUpdate();

			stmt.close();

			sb = new StringBuffer();

			//SQL文作成
			sb.append("UPDATE LOGIN SET ");
			sb.append("PASSWORD = ?,");
			sb.append("UPDATE_DT = ?");
			sb.append("WHERE USER_ID = ?");
			sb.append(";");

			stmt = conn.prepareStatement(sb.toString());

			//パラメータ埋め込み
			stmt.setString(1, password);
			stmt.setTimestamp(2, timestamp);
			stmt.setString(3, userid);

			//SQLの実行
			int loginCnt = stmt.executeUpdate();

			if (userCnt > 0 || loginCnt > 0) {
				conn.commit();
				filename = "/WEB-INF/userUpdateComplete.jsp";	//どちらかのテーブルが1件更新できたのでOK
			}
			else {
				//Loggerなどを使うとかっこいいが、とりあえずSystem.outで。
				System.out.println("更新対象のデータはありません");
				conn.rollback();
			}

		}
		catch (Exception e) {
			//rollback処理
			try {
				conn.rollback();
			} catch (Exception e2) {
				//処理のしようがないので、何もしない
			}
			e.printStackTrace();
		}
		finally {
			//close処理
			try {
				stmt.close();
				conn.close();
			}
			catch (Exception e)
			{
				//処理のしようがないので、何もしない
			}

		}

		//OKまたはNGへ遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher(filename);
		dispatcher.forward(request, response);
	}

}
