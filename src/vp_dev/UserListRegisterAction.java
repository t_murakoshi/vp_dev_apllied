package vp_dev;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UserListSearchAction
 */
@WebServlet("/UserListRegisterAction")
public class UserListRegisterAction extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListRegisterAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//do nothing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");	//POSTした値の文字コードをUTF-8にする

		request.getSession().removeAttribute("userlist");	//いらないので削除

		//登録データの初期化
		HashMap<String, String> userselected = new HashMap<String, String>();
		userselected.put("USER_ID", "");
		userselected.put("PASSWORD", "");
		userselected.put("LAST_NAME", "");
		userselected.put("FIRST_NAME", "");
		userselected.put("LAST_NAME_KANA", "");
		userselected.put("FIRST_NAME_KANA", "");
		userselected.put("AGE", "");

		request.getSession().setAttribute("userselected", userselected);

		//遷移
		String filename = "/UserRegisterInput";	//ユーザー新規登録入力画面
		//URLを切り替えたいので、リダイレクトする
		response.sendRedirect(request.getContextPath() + filename);
	}

}
