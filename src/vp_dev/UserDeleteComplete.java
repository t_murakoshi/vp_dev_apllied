package vp_dev;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Login
 */
@WebServlet("/UserDeleteComplete")
public class UserDeleteComplete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteComplete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//GETの利用は不可とする
		String filename = "/WEB-INF/UserDeleteNG.jsp";	//エラー画面
		RequestDispatcher dispatcher = request.getRequestDispatcher(filename);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");	//POSTした値の文字コードをUTF-8にする

		@SuppressWarnings("unchecked")
		HashMap<String, String> userselected = (HashMap<String, String> )request.getSession().getAttribute("userselected");

		String userid = userselected.get("USER_ID");		//セッション上のuserid

		String filename = "/WEB-INF/userDeleteNG.jsp";	//とりあえずNG画面に遷移するように設定
		Connection conn = null;	//finallyで処理するので、ここで定義
		PreparedStatement stmt = null;	//finallyで処理するので、ここで定義

		String dbuser = "root";
		String dbpassword = "password";	//★たぶん皆さんのパスワードはadminなので、adminに変えてください

		try {
			Class.forName("com.mysql.jdbc.Driver");

			//ConnectionとStatementの取得
			conn = DriverManager.getConnection("jdbc:mysql://localhost/VP_DEV?serverTimezone=JST&allowPublicKeyRetrieval=true&useSSL=false", dbuser, dbpassword);

			conn.setAutoCommit(false);

			StringBuffer sb = new StringBuffer();

			//SQL文作成
			sb.append("DELETE FROM USER ");
			sb.append("WHERE USER_ID = ?");
			sb.append(";");

			stmt = conn.prepareStatement(sb.toString());

			//パラメータ埋め込み
			stmt.setString(1, userid);

			//SQLの実行
			int userCnt = stmt.executeUpdate();

			stmt.close();

			sb = new StringBuffer();

			//SQL文作成
			sb.append("DELETE FROM LOGIN ");
			sb.append("WHERE USER_ID = ?");
			sb.append(";");

			stmt = conn.prepareStatement(sb.toString());

			//パラメータ埋め込み
			stmt.setString(1, userid);

			//SQLの実行
			int loginCnt = stmt.executeUpdate();

			if (userCnt > 0 || loginCnt > 0) {
				conn.commit();
				filename = "/WEB-INF/userDeleteComplete.jsp";	//どちらかのテーブルが1件削除できたのでOK
			}
			else {
				//Loggerなどを使うとかっこいいが、とりあえずSystem.outで。
				System.out.println("削除対象のデータはありません");
				conn.rollback();
			}

		}
		catch (Exception e) {
			//rollback処理
			try {
				conn.rollback();
			} catch (Exception e2) {
				//処理のしようがないので、何もしない
			}
			e.printStackTrace();
		}
		finally {
			//close処理
			try {
				stmt.close();
				conn.close();
			}
			catch (Exception e)
			{
				//処理のしようがないので、何もしない
			}

		}

		//OKまたはNGへ遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher(filename);
		dispatcher.forward(request, response);
	}

}
