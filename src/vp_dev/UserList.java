package vp_dev;


import java.io.IOException;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Login
 */
@WebServlet("/UserList")
public class UserList extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//メニューはaタグなので、GETで飛んでくる

		//セッション削除
		request.getSession().removeAttribute("userlist");
		request.getSession().removeAttribute("userselected");

		//初期化
		doInit(request, response);

		String filename = "/WEB-INF/userList.jsp";	//検索・一覧画面

		//遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher(filename);
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");	//POSTした値の文字コードをUTF-8にする

		if (request.getParameter("search") != null) {
			//別サーブレットに移動（別クラスに処理を切り出し）
			RequestDispatcher dispatcher = request.getRequestDispatcher("/UserListSearchAction");
			dispatcher.forward(request, response);
		} else if (request.getParameter("register") != null) {
			//別サーブレットに移動（別クラスに処理を切り出し）
			RequestDispatcher dispatcher = request.getRequestDispatcher("/UserListRegisterAction");
			dispatcher.forward(request, response);
		} else if (request.getParameter("update") != null) {
			//別サーブレットに移動（別クラスに処理を切り出し）
			RequestDispatcher dispatcher = request.getRequestDispatcher("/UserListUpdateAction");
			dispatcher.forward(request, response);
		} else if (request.getParameter("delete") != null) {
			//別サーブレットに移動（別クラスに処理を切り出し）
			RequestDispatcher dispatcher = request.getRequestDispatcher("/UserListDeleteAction");
			dispatcher.forward(request, response);
		} else {

			doGet(request, response);

		}

	}

	private void doInit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//検索条件の初期化
		HashMap<String, String> searchCondition = new HashMap<String, String>();
		searchCondition.put("USER_ID", "");
		searchCondition.put("LAST_NAME", "");
		searchCondition.put("FIRST_NAME", "");
		searchCondition.put("LAST_NAME_KANA", "");
		searchCondition.put("FIRST_NAME_KANA", "");
		searchCondition.put("AGE", "");

		request.setAttribute("searchCondition", searchCondition);

	}



}
