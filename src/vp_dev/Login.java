package vp_dev;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セキュリティ上、GETでのログインは不可とする（ログイン画面に強制的に戻す）
		String filename = "/WEB-INF/index.jsp";	//ログイン画面
		RequestDispatcher dispatcher = request.getRequestDispatcher(filename);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");	//POSTした値の文字コードをUTF-8にする

		String userid = request.getParameter("userid");		//画面上のuserid
		String password = request.getParameter("password");	//画面上のpassword

		String filename = "/WEB-INF/index.jsp";	//とりあえずログイン画面に遷移するように設定
		Connection conn = null;	//finallyで処理するので、ここで定義
		Statement stmt = null;	//finallyで処理するので、ここで定義
		ResultSet rs = null;	//finallyで処理するので、ここで定義

		String dbuser = "root";
		String dbpassword = "password";	//★たぶん皆さんのパスワードはadminなので、adminに変えてください

		boolean isLoginOK = false;

		try {
			Class.forName("com.mysql.jdbc.Driver");

			//ConnectionとStatementの取得
			conn = DriverManager.getConnection("jdbc:mysql://localhost/VP_DEV?serverTimezone=JST&allowPublicKeyRetrieval=true&useSSL=false", dbuser, dbpassword);
			stmt = conn.createStatement();

			//画面上の入力値とDBの値を突き合せるSQL
			String sql = "SELECT * FROM LOGIN "
					+ "WHERE USER_ID = '" + userid + "' "
					+ "AND PASSWORD = '" + password + "' ";

			//SQLの実行(SELECT)
			//★INSERT,UPDATE,DELETEの時はexecuteQuery()ではなく、executeUpdate()なので注意！
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				isLoginOK = true;
			}

		}
		catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", "システムエラーが発生しました");
		}
		finally {
			//close処理
			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			}
			catch (SQLException e)
			{
				//処理のしようがないので、何もしない
			}

		}

		//ログインOKまたはNGへ遷移
		if (isLoginOK) {
			filename = "/Menu";	//画面上の入力値とDBの値が一致したのでメニューに遷移
			//URLを切り替えたいので、リダイレクトする
			response.sendRedirect(request.getContextPath() + filename);
		} else {
			//forward
			request.setAttribute("msg", "ログインできませんでした");
			RequestDispatcher dispatcher = request.getRequestDispatcher(filename);
			dispatcher.forward(request, response);
		}

	}

}
