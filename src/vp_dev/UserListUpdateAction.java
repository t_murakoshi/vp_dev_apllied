package vp_dev;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UserListSearchAction
 */
@WebServlet("/UserListUpdateAction")
public class UserListUpdateAction extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListUpdateAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//do nothing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");	//POSTした値の文字コードをUTF-8にする

		//listをセッションから取得
		@SuppressWarnings("unchecked")
		ArrayList<HashMap<String, String>> list = (ArrayList<HashMap<String, String>> )request.getSession().getAttribute("userlist");

		request.getSession().removeAttribute("userlist");	//もういらないので削除

		int recordNo = Integer.parseInt(request.getParameter("userselect"));	//何行目のラジオボタンがチェックされたか
		request.getSession().setAttribute("userselected", list.get(recordNo));	//指定行のHashMapをセッションに格納

		//遷移
		String filename = "/UserUpdateInput";	//ユーザー編集入力画面
		//URLを切り替えたいので、リダイレクトする
		response.sendRedirect(request.getContextPath() + filename);

	}

}
