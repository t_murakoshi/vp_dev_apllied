package vp_dev;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Menu
 */
@WebServlet("/UserUpdateConfirm")
public class UserUpdateConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//GETの利用は不可とする
		String filename = "/WEB-INF/UserUpdateNG.jsp";	//エラー画面
		RequestDispatcher dispatcher = request.getRequestDispatcher(filename);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");	//POSTした値の文字コードをUTF-8にする

		@SuppressWarnings("unchecked")
		HashMap<String, String> userselected = (HashMap<String, String> )request.getSession().getAttribute("userselected");

		String password = request.getParameter("password");	//画面上のpassword
		String lastname = request.getParameter("lastname");	//画面上のlastname
		String firstname = request.getParameter("firstname");		//画面上のfirstname
		String lastnamekana = request.getParameter("lastnamekana");	//画面上のlastnamekana
		String firstnamekana = request.getParameter("firstnamekana");		//画面上のfirstnamekana
		String age = request.getParameter("age");	//画面上のage

		userselected.put("PASSWORD", password);
		userselected.put("LAST_NAME", lastname);
		userselected.put("FIRST_NAME", firstname);
		userselected.put("LAST_NAME_KANA", lastnamekana);
		userselected.put("FIRST_NAME_KANA", firstnamekana);
		userselected.put("AGE", age);

		//遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/userUpdateConfirm.jsp");
		dispatcher.forward(request, response);
	}

}
