package vp_dev;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UserListSearchAction
 */
@WebServlet("/UserListSearchAction")
public class UserListSearchAction extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListSearchAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//do nothing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");	//POSTした値の文字コードをUTF-8にする

		String userid = request.getParameter("userid");		//画面上のuserid
		String lastname = request.getParameter("lastname");	//画面上のlastname
		String firstname = request.getParameter("firstname");		//画面上のfirstname
		String lastnamekana = request.getParameter("lastnamekana");	//画面上のlastnamekana
		String firstnamekana = request.getParameter("firstnamekana");		//画面上のfirstnamekana
		String age = request.getParameter("age");	//画面上のage

		String filename = "/WEB-INF/userList.jsp";	//検索・一覧画面
		Connection conn = null;	//finallyで処理するので、ここで定義
		Statement stmt = null;	//finallyで処理するので、ここで定義
		ResultSet rs = null;	//finallyで処理するので、ここで定義

		String dbuser = "root";
		String dbpassword = "password";	//★たぶん皆さんのパスワードはadminなので、adminに変えてください

		try {
			Class.forName("com.mysql.jdbc.Driver");

			//ConnectionとStatementの取得
			conn = DriverManager.getConnection("jdbc:mysql://localhost/VP_DEV?serverTimezone=JST&allowPublicKeyRetrieval=true&useSSL=false", dbuser, dbpassword);
			stmt = conn.createStatement();

			//検索SQL
			String sql = "SELECT U.*, L.* FROM USER U, LOGIN L ";

			//WHERE条件
			ArrayList<String> whereClauses = new ArrayList<String>();

			whereClauses.add("U.USER_ID = L.USER_ID");

			if (userid != null && !userid.isEmpty()) {
				whereClauses.add("U.USER_ID = '" + userid + "'");
			}
			if (lastname != null && !lastname.isEmpty()) {
				whereClauses.add("U.LAST_NAME = '" + lastname + "'");
			}
			if (firstname != null && !firstname.isEmpty()) {
				whereClauses.add("U.FIRST_NAME = '" + firstname + "'");
			}
			if (lastnamekana != null && !lastnamekana.isEmpty()) {
				whereClauses.add("U.LAST_NAME_KANA = '" + lastnamekana + "'");
			}
			if (firstnamekana != null && !firstnamekana.isEmpty()) {
				whereClauses.add("U.FIRST_NAME_KANA = '" + firstnamekana + "'");
			}
			if (firstnamekana != null && !firstnamekana.isEmpty()) {
				whereClauses.add("U.FIRST_NAME_KANA = '" + firstnamekana + "'");
			}
			if (age != null && !age.isEmpty()) {
				whereClauses.add("U.AGE = '" + age + "'");
			}

			String whereClause = String.join(" AND ", whereClauses);
			if (whereClause != null && !whereClause.isEmpty()) {
				sql = sql + " WHERE " + whereClause;
			}
			sql = sql + ";";

			//SQLの実行
			rs = stmt.executeQuery(sql);

			//検索条件をHashMapに格納
			HashMap<String, String> searchCondition = new HashMap<String, String>();
			searchCondition.put("USER_ID", userid);
			searchCondition.put("LAST_NAME", lastname);
			searchCondition.put("FIRST_NAME", firstname);
			searchCondition.put("LAST_NAME_KANA", lastnamekana);
			searchCondition.put("FIRST_NAME_KANA", firstnamekana);
			searchCondition.put("AGE", age);

			request.setAttribute("searchCondition", searchCondition);

			//結果をListに格納
			ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

			while (rs.next()) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("USER_ID", rs.getString("USER_ID"));
				map.put("PASSWORD", rs.getString("PASSWORD"));
				map.put("LAST_NAME", rs.getString("LAST_NAME"));
				map.put("FIRST_NAME", rs.getString("FIRST_NAME"));
				map.put("LAST_NAME_KANA", rs.getString("LAST_NAME_KANA"));
				map.put("FIRST_NAME_KANA", rs.getString("FIRST_NAME_KANA"));
				map.put("AGE", rs.getString("AGE"));
				list.add(map);
			}

			request.getSession().setAttribute("userlist", list);

		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			//close処理
			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			}
			catch (SQLException e)
			{
				//処理のしようがないので、何もしない
			}

		}

		//遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher(filename);
		dispatcher.forward(request, response);

	}

}
