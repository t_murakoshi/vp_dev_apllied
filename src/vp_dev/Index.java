package vp_dev;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Index
 */
@WebServlet("/")
public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Index() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//最初は通常GETで来るので、GETにロジックを記載

		//全セッション削除
		Enumeration<String> vals = request.getSession().getAttributeNames();

		while(vals.hasMoreElements()){
			String nm = (String)vals.nextElement();
			request.getSession().removeAttribute(nm);
		}

		if (!request.getServletPath().equals("/")) {
			//間違ったURLを直打ちされた場合
			//URLが間違ってます的な画面に遷移したほうがスマートだが、とりあえず。
			throw new ServletException("不正なパス");
		}

		//遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/index.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
